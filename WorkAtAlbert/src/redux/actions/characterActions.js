import {
  GET_CHARACTER_START,
  FETCHING_CHARACTER_SUCCESS,
  FETCHING_CHARACTER_FAILURE,
} from '../constants';

export const startCharacter = () => {
  return {type: GET_CHARACTER_START};
};

export const getCharacterSuccess = data => {
  return {type: FETCHING_CHARACTER_SUCCESS, data};
};

export const getCharacterFailure = error => {
  return {type: FETCHING_CHARACTER_FAILURE, error};
};

export const getCharacter = url => {
  return dispatch => {
    dispatch(startCharacter());
    let characterArr = [];
    fetch(url)
      .then(response => response.json())
      .then(result => {
        characterArr.push(result.name);
        characterArr.push(result.gender);
        const urlSpecie = result.species[0];
        return fetch(urlSpecie)
          .then(response => response.json())
          .then(specie => {
            characterArr.push(specie.name);
            dispatch(getCharacterSuccess(characterArr));
          });
      })
      .catch(err => {
        dispatch(getCharacterFailure(err));
      });
  };
};
