import {
  GET_FILMS_START,
  FETCHING_FILMS_SUCCESS,
  FETCHING_FILMS_FAILURE,
} from '../constants';

import {API_REST_URI, FILMS_PATH} from '../../api/paths';

export const startFilms = () => {
  return {type: GET_FILMS_START};
};

export const getFilmsSuccess = data => {
  return {type: FETCHING_FILMS_SUCCESS, data};
};

export const getFilmsFailure = error => {
  return {type: FETCHING_FILMS_FAILURE, error};
};

export const getAllFilms = () => {
  return dispatch => {
    dispatch(startFilms());
    const url = API_REST_URI + FILMS_PATH;
    fetch(url)
      .then(response => response.json())
      .then(results => {
        dispatch(getFilmsSuccess(results));
      })
      .catch(err => {
        dispatch(getFilmsFailure(err));
      });
  };
};
