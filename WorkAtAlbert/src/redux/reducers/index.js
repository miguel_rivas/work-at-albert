import {combineReducers} from 'redux';

import getFilmsReducer from './getFilms.reducer';
import getCharacterReducer from './getCharacter.reducer';

const appReducer = combineReducers({
  getFilms: getFilmsReducer,
  getCharacter: getCharacterReducer,
});

export default appReducer;
