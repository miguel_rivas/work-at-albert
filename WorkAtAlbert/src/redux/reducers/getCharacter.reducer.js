import {
  GET_CHARACTER_START,
  FETCHING_CHARACTER_SUCCESS,
  FETCHING_CHARACTER_FAILURE,
  CLEAN_CHARACTER,
} from '../constants';

export const initialState = {
  data: null,
  isFetching: false,
  error: false,
};

const dataReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CHARACTER_START:
      return {
        ...state,
        data: [],
        isFetching: true,
        error: false,
      };
    case FETCHING_CHARACTER_SUCCESS:
      return {
        ...state,
        data: [...state.data, action.data],
        isFetching: false,
        error: false,
      };
    case FETCHING_CHARACTER_FAILURE:
      return {
        ...state,
        data: action.error,
        isFetching: false,
        error: true,
      };
    case CLEAN_CHARACTER:
      return {
        ...state,
        data: [],
        isFetching: false,
        error: false,
      };
    default:
      return state;
  }
};

export default dataReducer;
