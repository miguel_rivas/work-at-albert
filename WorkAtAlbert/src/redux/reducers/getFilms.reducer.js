import {
  GET_FILMS_START,
  FETCHING_FILMS_SUCCESS,
  FETCHING_FILMS_FAILURE,
} from '../constants';

export const initialState = {
  data: null,
  isFetching: false,
  error: false,
};

const dataReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_FILMS_START:
      return {
        ...state,
        data: [],
        isFetching: true,
        error: false,
      };
    case FETCHING_FILMS_SUCCESS:
      return {
        ...state,
        data: action.data,
        isFetching: false,
        error: false,
      };
    case FETCHING_FILMS_FAILURE:
      return {
        ...state,
        data: action.error,
        isFetching: false,
        error: true,
      };
    default:
      return state;
  }
};

export default dataReducer;
