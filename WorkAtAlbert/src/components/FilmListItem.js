import React, {useRef, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Animated,
  Easing,
} from 'react-native';
import starWarsTheme from '../styles/common';
const windowWidth = Dimensions.get('window').width;

const FilmListItem = ({
  title,
  episode,
  release_date,
  characters,
  index,
  navigation,
}) => {
  const translationY = useRef(new Animated.Value(-50)).current;
  const fadeAnim = useRef(new Animated.Value(0)).current;
  // Animation
  useEffect(() => {
    Animated.timing(translationY, {
      delay: 500,
      toValue: 10,
      easing: Easing.bounce,
      useNativeDriver: true,
    }).start();
    Animated.timing(fadeAnim, {
      toValue: 1,
      delay: 500,
      duration: 500,
      useNativeDriver: true,
    }).start();
  }, []);

  const handlePress = () => {
    navigation.navigate('DetailFilm', {
      filmId: index,
      title: title,
      episode: episode,
      release_date: release_date,
      characters: characters,
    });
  };

  return (
    <TouchableOpacity onPress={() => handlePress()}>
      <Animated.View
        style={[
          styles.film,
          styles.boxWithShadow,
          {opacity: fadeAnim},
          {transform: [{translateY: translationY}]},
        ]}>
        <Text style={styles.title}>{title}</Text>
        <View style={styles.episodeContainer}>
          <View>
            <View style={[styles.lineDecoration, {marginBottom: 3}]} />
            <View style={styles.lineDecoration} />
          </View>
          <Text style={styles.textDesc}>Episode {episode}</Text>
          <View>
            <View style={[styles.lineDecoration, {marginBottom: 3}]} />
            <View style={styles.lineDecoration} />
          </View>
        </View>
        <Text style={styles.released}>Released on</Text>
        <Text style={styles.year}>{release_date}</Text>
      </Animated.View>
    </TouchableOpacity>
  );
};
export default FilmListItem;

const styles = StyleSheet.create({
  film: {
    width: windowWidth - 20,
    height: 180,
    backgroundColor: '#1A1C20',
    borderRadius: 16,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    padding: 5,
  },
  boxWithShadow: {
    shadowColor: '#E6E7EA',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
  title: {
    color: starWarsTheme.primaryColor,
    fontSize: 30,
    paddingHorizontal: 5,
    fontFamily: starWarsTheme.fontFamilyExtraBold,
    textAlign: 'center',
  },
  episodeContainer: {
    width: windowWidth - 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  textDesc: {
    color: starWarsTheme.primaryColor,
    fontSize: 22,
    fontFamily: starWarsTheme.fontFamilyRegular,
  },
  lineDecoration: {
    width: 100,
    borderColor: starWarsTheme.primaryColor,
    borderWidth: 1,
  },
  released: {
    color: 'white',
    fontSize: 16,
    fontFamily: starWarsTheme.fontFamilyRegular,
  },
  year: {
    color: starWarsTheme.primaryColor,
    fontFamily: starWarsTheme.fontFamilyBold,
    fontSize: 20,
  },
});
