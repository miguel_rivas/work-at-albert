import React, {useState, useEffect, useRef} from 'react';
import {View, SafeAreaView, StyleSheet, FlatList, Animated} from 'react-native';
import {useSelector} from 'react-redux';
import FilmListItem from '../components/FilmListItem';
import starWarsTheme from '../styles/common';

function HomeScreen({navigation}) {
  const [films, setFilms] = useState([]);
  const filmsData = useSelector(state => state.getFilms);
  const fadeAnim = useRef(new Animated.Value(0)).current;

  // Fetch Data
  useEffect(() => {
    function grabJustFive(charactersArr) {
      let top5characters = [];
      for (let index = 0; index < 5; index++) {
        top5characters.push(charactersArr[index]);
      }
      return top5characters;
    }
    if (filmsData && filmsData.data.results !== undefined) {
      filmsData.data.results.forEach((film, i) => {
        const filmObj = {
          title: film.title,
          episode: film.episode_id,
          release_date: film.release_date.substring(0, 4),
          characters: grabJustFive(film.characters),
          index: i,
        };
        setFilms(oldArray => [...oldArray, filmObj]);
      });
    }
  }, [filmsData]);

  // Animation
  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      delay: 500,
      duration: 500,
      useNativeDriver: true,
    }).start();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      {filmsData && filmsData.isFetching === true ? (
        <View style={styles.container}>
          <Animated.Text style={[styles.title, {opacity: fadeAnim}]}>
            MAYBE I BELONG AMONG THE STARS
          </Animated.Text>
        </View>
      ) : (
        <FlatList
          data={films}
          style={styles.flatList}
          keyExtractor={(item, index) => index.toString()}
          horizontal={false}
          showsVerticalScrollIndicator={false}
          renderItem={({item, index}) => (
            <FilmListItem
              title={item.title}
              episode={item.episode}
              release_date={item.release_date}
              index={item.index}
              characters={item.characters}
              navigation={navigation}
            />
          )}
          ItemSeparatorComponent={() => <View style={styles.separator} />}
        />
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  flatList: {
    flexGrow: 0,
    marginVertical: 10,
  },
  title: {
    color: starWarsTheme.primaryColor,
    fontSize: 30,
    paddingHorizontal: 5,
    fontFamily: starWarsTheme.fontFamilyExtraBold,
    textAlign: 'center',
  },
  separator: {marginVertical: 10},
});

export default HomeScreen;
