import React, {useEffect, useRef} from 'react';
import {View, Animated, StyleSheet, Dimensions} from 'react-native';
import {useDispatch} from 'react-redux';
import {getAllFilms} from '../redux/actions/filmsActions';
import starWarsTheme from '../styles/common';

const windowWidth = Dimensions.get('window').width;

const SplashScreen = ({navigation}) => {
  const translationLeft = useRef(new Animated.Value(-windowWidth)).current;
  const translationRight = useRef(new Animated.Value(windowWidth * 2)).current;
  const fadeAnim = useRef(new Animated.Value(0)).current;
  const dispatch = useDispatch();

  // get all films
  useEffect(() => {
    dispatch(getAllFilms());
  }, []);

  // Animation
  useEffect(() => {
    Animated.timing(translationLeft, {
      toValue: 0,
      delay: 250,
      duration: 650,
      useNativeDriver: true,
    }).start();
    Animated.timing(translationRight, {
      toValue: 0,
      delay: 250,
      duration: 650,
      useNativeDriver: true,
    }).start();
    Animated.timing(fadeAnim, {
      toValue: 1,
      delay: 1000,
      duration: 1500,
      useNativeDriver: true,
    }).start();
  }, []);

  // Navigation
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('Home');
    }, 4000);
  }, []);

  return (
    <View style={styles.container}>
      <Animated.Text
        style={[
          styles.textTitle,
          {transform: [{translateX: translationLeft}]},
        ]}>
        STAR{' '}
      </Animated.Text>
      <View style={styles.separator} />
      <Animated.Text
        style={[
          styles.textTitle,
          {transform: [{translateX: translationRight}]},
        ]}>
        WARS
      </Animated.Text>
      <Animated.Text style={[styles.slogan, {opacity: fadeAnim}]}>
        May the Force be with you
      </Animated.Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: starWarsTheme.secondaryColor,
  },
  textTitle: {
    color: starWarsTheme.primaryColor,
    fontSize: 80,
    fontFamily: starWarsTheme.fontFamilyExtraBold,
  },
  separator: {
    width: windowWidth - 20,
    height: 2,
    backgroundColor: starWarsTheme.primaryColor,
  },
  slogan: {
    color: starWarsTheme.primaryColor,
    fontSize: 22,
    fontFamily: starWarsTheme.fontFamilyRegular,
  },
});

export default SplashScreen;
