/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Animated,
  Dimensions,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getCharacter} from '../redux/actions/characterActions';

import starWarsTheme from '../styles/common';
const windowWidth = Dimensions.get('window').width;

const DetailFilmScreen = ({route}) => {
  const {filmId, title, episode, release_date, characters} = route.params;
  const [characterDescription, setCharacterDescription] = useState([]);
  const translation = useRef(new Animated.Value(200)).current;
  const dispatch = useDispatch();
  const characterData = useSelector(state => state.getCharacter);

  // Fetch Data
  useEffect(() => {
    for (let i = 0; i < characters.length; i++) {
      dispatch(getCharacter(characters[i]));
    }
  }, []);

  // udpate UI
  useEffect(() => {
    if (characterData.data) {
      setCharacterDescription(characterData.data);
    }
  },[characterData]);

  // Animations
  useEffect(() => {
    Animated.timing(translation, {
      toValue: 0,
      delay: 1000,
      useNativeDriver: true,
    }).start();
  }, []);

  const handlePoster = () => {
    switch (episode) {
      case 4:
        return require('../../assets/images/ANewHope.png');
      case 5:
        return require('../../assets/images/TheEmpireStrikesBack.png');
      case 6:
        return require('../../assets/images/ReturnOfTheJedi.png');
      case 1:
        return require('../../assets/images/ThePhantomMenace.png');
      case 2:
        return require('../../assets/images/AttackOfTheClones.png');
      case 3:
        return require('../../assets/images/RevengeOfTheSith.jpg');
      case 7:
        return require('../../assets/images/TheForceAwakens.png');
      default:
        break;
    }
  };

  return (
    <ImageBackground source={handlePoster()} style={styles.imageBG}>
      <View style={styles.container}>
        <Animated.View
          style={[styles.card, {transform: [{translateY: translation}]}]}>
          <Text style={styles.title}>{title}</Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={styles.textDesc}>Episode: {episode}</Text>
            <Text style={styles.textDesc}>Relesead on {release_date}</Text>
          </View>
          <View style={styles.listTitleContainer}>
            <Text style={styles.listTitle}>Characters </Text>
            <Text style={styles.listTitle}>Gender</Text>
            <Text style={styles.listTitle}>Species</Text>
          </View>
          {characterData && characterData.isFetching === true ? (
            <ActivityIndicator
              size={'large'}
              visible={characterData.isFetching}
              color="white"
              style={{
                paddingVertical: 50,
              }}
            />
          ) : (
            <FlatList
              data={characterDescription}
              style={styles.flatList}
              keyExtractor={(item, index) => index.toString()}
              horizontal={false}
              showsVerticalScrollIndicator={false}
              renderItem={({item, index}) => (
                <View
                  style={{
                    flexDirection: 'row',
                    //width: windowWidth - 10,
                    justifyContent: 'space-between',
                  }}>
                  <Text style={styles.listTitle}>{item[0]}</Text>
                  <Text style={styles.listTitle}>{item[1]}</Text>
                  <Text style={styles.listTitle}>{item[2]}</Text>
                </View>
              )}
            />
          )}
        </Animated.View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  imageBG: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  card: {
    flex: 0.4,
    backgroundColor: starWarsTheme.secondaryColor,
    opacity: 0.85,
    padding: 10,
  },
  title: {
    color: starWarsTheme.primaryColor,
    fontFamily: starWarsTheme.fontFamilyExtraBold,
    fontSize: 28,
  },
  textDesc: {
    color: starWarsTheme.primaryColor,
    fontSize: 18,
    fontFamily: starWarsTheme.fontFamilyBold,
  },
  listTitleContainer: {
    width: windowWidth - 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: 'white',
  },
  listTitle: {
    color: 'white',
    fontSize: 16,
    marginTop: 20,
    fontFamily: starWarsTheme.fontFamilyBold,
  },
  flatList: {
    flexGrow: 0,
    marginVertical: 10,
  },
});
export default DetailFilmScreen;
