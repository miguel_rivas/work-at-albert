const starWarsTheme = {
  primaryColor: '#FCBD00', //yellow
  secondaryColor: '#1C1E22', //black
  fontFamilyThin: 'LibreFranklin-Thin',
  fontFamilyRegular: 'LibreFranklin-Regular',
  fontFamilyBold: 'LibreFranklin-Bold',
  fontFamilyExtraBold: 'LibreFranklin-ExtraBold',
};

export default starWarsTheme;
