// useFetch.js
import {useState, useEffect} from 'react';

export default function useFetch(url) {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(result => setData(result))
      .catch(e => console.log(e));
  }, []);

  return data;
}
