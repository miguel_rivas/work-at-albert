// full list of movies
export const API_REST_URI = 'https://swapi.py4e.com/api';
export const FILMS_PATH = '/films';
export const CHARACTERS_PATH = '/people/';
