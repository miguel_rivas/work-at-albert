# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Its a test as a part of HejAlbert process selection

### The task
* Your main task is to integrate a web site (React) or an app (React Native) with the external Star Wars API (https://swapi.py4e.com/)

* Create an app from a template or from scratch. Use React or React Native
* Set up integration with the external Star Wars API
* Display a list of Movies (Title and Episode number) and what year it was released
* Make a transition to a movie view, where you display Title, Episode number, Release date, Top 5 characters Name, Gender and Species
* Please also upload a short video showing the final results on a device or simulator.



### Where I can see the app running ? ###

* Click on this link to see a demo 
* https://www.loom.com/share/19859f80e4194be58e0c7bfcaf039147

